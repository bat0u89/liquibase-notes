# liquibase-notes

## update for postgres

liquibase --changeLogFile=changelog/changelog_master_upgrade.xml --driver=org.postgresql.Driver --url=jdbc:postgresql://localhost:5432/t_module --logLevel=debug --username=postgres --password=postgres update -Dtesting=true

## rollback for postgres

liquibase --changeLogFile=db/changelog/db.changelog-master.yaml --driver=org.postgresql.Driver --url=jdbc:postgresql://localhost:5432/o --logLevel=debug --username=postgres --password=postgres rollbackCount 1

## update for mssql

liquibase --changeLogFile=changelog/changelog_master_upgrade.xml --driver=com.microsoft.sqlserver.jdbc.SQLServerDriver --url=jdbc:postgresql://localhost:5432/t_module --logLevel=debug --username=postgres --password=postgres update -Dtesting=true

## rollback for mssql

liquibase --changeLogFile=db/changelog/db.changelog-master.yaml --driver=com.microsoft.sqlserver.jdbc.SQLServerDriver --url=jdbc:postgresql://localhost:5432/o --logLevel=debug --username=postgres --password=postgres rollbackCount 1

# run on existing database to generate the sql for the update instead of doing the actual update (postgres)

liquibase --changeLogFile=changelog/changelog_master_upgrade.xml --driver=org.postgresql.Driver --url=jdbc:postgresql://localhost:5432/t_module_ppd --username=postgres --password=postgres updateSql > "C:\Users\olegk.sotnikov\Desktop\update_log_.sql"

